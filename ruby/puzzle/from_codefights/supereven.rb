=begin



Let a number be supereven if all of its digits are even.

Here are the first few supereven positive integers:
2, 4, 6, 8, 20, 22, 24, 26, 28, 40, ...

Given an integer k, find the kth (1-based) supereven positive integer.

Example

For k = 6, the output should be
supereven(k) = 22.

The 6th supereven positive integer is 22.

Input/Output

    [time limit] 4000ms (rb)

    [input] integer k

    Constraints:
    1 ≤ k ≤ 19 · 105.

    [output] integer

    The kth (1-based) supereven positive integer.



=end


# RHAAAAAA 1/2h pour réaliser ce que signifie la question :/
# Et encore y'a plus simple, on peut multiplier par deux ... 0 -> 0; 1->2, 2->4, 3->6, 4->8....

def supereven(k)
  r =''
  base = '02468'
  s = k.to_s(5);
  s.length.times { |x|  r += base[s[x].to_i] }
  r.to_i
end
