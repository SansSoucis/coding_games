=begin

n a game we called that "hoop", n player numbered from 1 to n are playing. The first player says 1, the second player says 2 and so on. After everyone says a number, it's the first player's turn again. There's an additional rule that make the game more interesting: if the current number the person should say is divisible by 3 or 7, the person should say 0 instead.

You're given the number of players n, the number of turns m the game was played, and what was said in each turn. Your task is to find the players who made mistakes.

    the players should be returned in the order they made mistakes;
    if the player made several mistakes, all of them should be returned.

Example

For n = 3, m = 8 and turns = [1, 2, 3, 4, 4, 0, 0, 0],
the output should be
hoop(n, m, turns) = [3, 2, 2].

During the first round, players 1 and 2 did everything right, but the third player forgot to say 0 (3 is divisible by 3, so 0 should be said instead).
During the second round, player 2 said 4 instead of 5, which was a mistake.
Finally, during the last round the second player said 0 instead of 8, which was a mistake.

Input/Output

    [time limit] 4000ms (rb)

    [input] integer n

    The number of players.

    Constraints:
    1 ≤ n ≤ 10.

    [input] integer m

    The number of turns.

    Constraints:
    1 ≤ m ≤ 50.

    [input] array.integer turns

    Array of non-negative numbers, where the ith number is the number said in the ith turn.

    Constraints:
    turns.length = m.

    [output] array.integer

    Array of players who made mistakes in the order of turns the mistakes were made in.


=end



# returns the expected output
def hoop(n, m, turns)
  out=[]
  gamer= 1
  turns.each_with_index do  |answer, index |
    out << gamer unless answer == good_answer(index+1)
    (gamer == n) ? gamer = 1 : gamer += 1
  end
  out
end


# returns the expected answer on the "number" turn
# @param [integer] number
def good_answer (number)
  (number%3 == 0 || number%7 == 0) ? 0 : number
end


