=begin


Consider that the English alphabet contains 26 characters, while telephones only have ten digits on the keypad.
The letters are mapped onto the digits as shown below:

When you press a digit, the corresponding letter appears on the screen.
If you keep pressing the button without pauses, the letters mapped onto the button change in sequence.
Let's assume that no button is pressed repeatedly more times than there are letters mapped onto it.

Given the list of digits pressed, return the text that should appear on the screen.

Example

    For keys = "44 444", the output should be
    reverse_t9(keys) = "hi".

    For keys = "999337777", the output should be
    reverse_t9(keys) = "yes".


Input/Output

-    [time limit] 4000ms (rb)

-   [input] string keys

-    Sequence of key presses, a string of digits (all but '1') and whitespace characters, where a whitespace character means a pause. It is guaranteed that there are no two consecutive '0's or ' 's in the string.

-    Constraints:
    1 ≤ keys.length ≤ 1500.

=end


CONTEXT = "DEV"
@keyboard = {
    '0'   => ' ',
    '2'   => 'abc',
    '3'   => 'def',
    '4'   => 'ghi',
    '5'   => 'jkl',
    '6'   => 'mno',
    '7'   => 'pqrs',
    '8'   => 'tuv',
    '9'   => 'wxyz',
    ' ' => ''
}

def reverse_t9(keys)
  out = String.new
  previous_characters = nil
  keys.each_char do |character|
    if  previous_characters
      if previous_characters[0] == character
        previous_characters << character
      else
        out_char = key_sequence_to_letter(previous_characters)
        out += out_char if out_char
        previous_characters = character
      end
    else
      previous_characters = character
    end
  end
  out += key_sequence_to_letter(previous_characters)
  out
end

def key_sequence_to_letter(sequence)
  @keyboard[sequence[0]][sequence.length - 1] if sequence
end


if CONTEXT == "DEV"
  #Entertainment production...
  require 'test/unit/assertions'
  include Test::Unit::Assertions

  require 'benchmark'

  Benchmark.bm do |bm|
    puts "RUNNING TESTS \n"
    bm.report {

      puts "\nTest 1 ok" if assert_equal 'hi', reverse_t9("44 444")
      puts "Test 2 ok" if assert_equal 'yes', reverse_t9("999337777")
      puts "Test 3 ok" if assert_equal 'no', reverse_t9("66 666")
      puts "Test 4 ok" if assert_equal 'ha ha ha', reverse_t9("44204420442")
      puts "Test 5 ok" if assert_equal 'aaaaaaaaa', reverse_t9("2 2 2 2 2 2 2 2 2")
      puts "Test 6 ok" if assert_equal 'bbbbbbbbb', reverse_t9("22 22 22 22 22 22 22 22 22")
      puts "Test 7 ok" if assert_equal 'abcdefghijklmno', reverse_t9("2 22 2223 33 3334 44 4445 55 5556 66 666")

      puts reverse_t9("8337777803388833777999844 4446640999666880222 2660000000000844 4446655066633308444555 555044480777886677770477733 3366")
    }
  end


end

