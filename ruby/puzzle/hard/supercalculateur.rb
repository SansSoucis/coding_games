@calculs =[]
@n = gets.to_i
@n.times do
  j, d = gets.split(" ").collect {|x| x.to_i}
  @calculs << [j,j + d-1]
end

candidate =  @calculs.sort_by {|calcul| [calcul[0],calcul[1]] }

stack=[]
current = candidate[0]
stack << current



candidate.each do |candidat|
  if ( candidat[0] > current[1] )
    stack << current  unless stack.last == current

    current = candidat
    stack << candidat
  end

  if ( candidat[1] < current[1]  )
    stack.pop
    current = candidat
    stack << candidat
  end
end

puts "#{stack.length}"



