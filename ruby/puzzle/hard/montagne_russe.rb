@queue = []
@l, @c, @n = gets.split(" ").collect {|x| x.to_i}
@n.times do
  @queue << gets.to_i
end

@memo ={} # where we store, for each index in @queue : [total earnings, next queue index] 
@cash = 0

#  Memo-ize the earnings. aka dynamic programing.
#  puts in "memo", for a given index in @queue :
#    - earnings for a turn commencing with @queue[index] group, 
#    - index of group in front of the queue on next turn.
#  memo[index] = [earning, next_turn_front_line_index]

def memoize_earnings
  @queue.each_with_index do |group,group_index|

    available_sits  = @l
    index = group_index
    @n.times do
      group_size = @queue[index]
      break if available_sits < group_size
      available_sits -= group_size
      index = next_index_after index
    end
    @memo[group_index] = [@l - available_sits, index]
  end

end

def next_index_after index
  index < @n - 1 ? index + 1 : 0
end


def run_a_day

  turn = @cash = index = 0
  while turn < @c
    @cash += @memo[index][0]
    index = @memo[index][1]
    turn += 1
  end
end

memoize_earnings
run_a_day


puts "#{@cash}"